"""
References:
    1. http://linas.org/art-gallery/escape/escape.html
    2. https://www.codingame.com/playgrounds/2358/how-to-plot-the-mandelbrot-set
"""

from math import log2
from PIL import Image, ImageDraw, ImageColor
from typing import Tuple, List
import argparse
import sys


def parse_arguments(argv: List[str]):
    parser = argparse.ArgumentParser(description="Specify parameters for calculation")
    parser.add_argument(
        "--escape_radius",
        type=float,
        default=20.0,
        required=False,
        help="The number after which we assume the calculation has become unbounded (going to inf)",
    )
    parser.add_argument(
        "--max_iter",
        type=int,
        default=20,
        required=False,
        help="The maximum number of iterations after which we move to next point of calculation",
    )
    parser.add_argument(
        "--image_x",
        type=int,
        default=600,
        required=False,
        help="Width of the image in pixels",
    )
    parser.add_argument(
        "--image_y",
        type=int,
        default=600,
        required=False,
        help="Height of the image in pixels",
    )
    parser.add_argument(
        "--origin_x",
        type=int,
        required=False,
        help="X location of the center of the mandelbrot; 0 is left, positive rightwards",
    )
    parser.add_argument(
        "--origin_y",
        type=int,
        required=False,
        help="Y location of the center of the mandelbrot; 0 is top, positive downwards",
    )
    parser.add_argument(
        "--zoom",
        type=int,
        default=200,
        required=False,
        help="Zoom - a proportional number",
    )
    args = parser.parse_args(argv)
    if args.origin_x is None:
        args.origin_x = args.image_x * 3 / 4
    if args.origin_y is None:
        args.origin_y = args.image_y / 2
    escape_radius, max_iter, image_size, origin, zoom = (
        args.escape_radius,
        args.max_iter,
        (args.image_x, args.image_y),
        (args.origin_x, args.origin_y),
        args.zoom,
    )
    return escape_radius, max_iter, image_size, origin, zoom


class Mandelbrot:
    def __init__(self, escape_radius, max_iter) -> None:
        self.escape_radius = escape_radius
        self.max_iter = max_iter
        self.color_stages = None
        self._colormap_calculated = list()

    def calculate(self, c: complex) -> float:
        z = complex(0)
        iter_count = 0
        while iter_count < self.max_iter and abs(z) <= self.escape_radius:
            z = z * z + c
            iter_count += 1

        return (
            iter_count
            if iter_count == self.max_iter
            else iter_count - log2(log2(abs(z)))
        )

    def colormap_lookup_rgb(self, mu: float) -> str:
        """
        Simple RGB
        """
        red = int(255 * mu / self.max_iter)
        green = 255
        blue = 255 if mu < self.max_iter else 0
        return f"rgb({red}, {green}, {blue})"

    def colormap_lookup_hsv(self, mu: float) -> str:
        """Simple HSV"""

        hue = 360 * mu / self.max_iter
        saturation = 100
        value = 100 if mu < self.max_iter else 0
        return f"hsv({hue}, {saturation}%, {value}%)"

    def colormap_lookup_custom(
        self, mu: float, color_stages: Tuple[Tuple[int, int, int], ...]
    ) -> str:
        """Custom gradients"""
        if len(color_stages) <= 1:
            raise RuntimeError("must have more than 1 color stage")
        if color_stages != self.color_stages:
            self.color_stages = color_stages
            # Calculate colormap
            self._colormap_calculated = [color_stages[0]]
            for i in range(1, len(color_stages)):
                diff = tuple(
                    map(lambda i, j: i - j, color_stages[i], color_stages[i - 1])
                )
                max_steps = max(map(lambda i: abs(i), diff))
                step_size = tuple(map(lambda i: i / max_steps, diff))
                step = color_stages[i - 1]
                for i in range(max_steps):
                    step = tuple(map(lambda i, j: i + j, step, step_size))
                    self._colormap_calculated.append(
                        tuple(map(lambda i: round(i), step))
                    )
            self._colormap_calculated.append((0, 0, 0))
        index = int((len(self._colormap_calculated) - 1) * mu / self.max_iter)
        red = self._colormap_calculated[index][0]
        green = self._colormap_calculated[index][1]
        blue = self._colormap_calculated[index][2]
        return f"rgb({red}, {green}, {blue})"


def main(argv):
    escape_radius, max_iter, image_size, origin, zoom = parse_arguments(argv)
    mandelbrot = Mandelbrot(escape_radius, max_iter)
    img = Image.new("RGB", image_size, (0, 0, 0))
    draw = ImageDraw.Draw(img)

    color_stages = (
        (0, 0, 0),
        (0, 0, 255),
        (0, 128, 0),
        (255, 255, 0),
        (255, 0, 0),
        (128, 0, 0),
        (128, 0, 128),
    )

    for x in range(0, image_size[0]):
        for y in range(0, image_size[1]):
            c = complex((x - origin[0]) / zoom, (y - origin[1]) / zoom)
            mu = mandelbrot.calculate(c)
            draw.point([x, y], fill=mandelbrot.colormap_lookup_custom(mu, color_stages))
            # draw.point((x, y), fill=mandelbrot.colormap_lookup_rgb(mu))

    img.show()


if __name__ == "__main__":
    main(sys.argv[1:])
