Python module to draw mandelbrot with zoom capability.
=======
# Mandelbrot
Draw mandelbrot with zoom capability

# How to run
```
$ python main.py

$ python main.py --zoom 3000 --origin_y 2500 --max_iter 100
```

![Output](mandelbrot.png)
![Output](mandelbrot-2.png)

# TODO
1. ~~Draw mandelbrot~~
1. ~~Change to a blue/black color palette similar to images at http://linas.org/art-gallery/escape/escape.html~~
1. ~Add zooming functionality~
1. Add on-demand zoom functionality
1. Convert to client-side javascript
1. Use clients to calculate zoomed values and store them on server
1. Maintain stored max and min zoom for whole range, and serve from server if within this range, otherwise use client to
   calculate further values and update max/min accordingly
1. Add finer control of colormap calculations (support custom step size, math functions)
1. Add multi threaded rendering